class Player extends GameObject {

    Awake(params) {
        this.radius = 5
        this.speed = params.speed || 5
        this.SetTarget(params.target)
        
        //Actions
        this.followTarget = params.followTarget || false

        this.showHypotenuse = params.showHypotenuse || params.showAll || false
        this.showOppositeCato = params.showOppositeCato || params.showAll || false
        this.showAdjacentCato = params.showAdjacentCato || params.showAll || false
        this.showAngle = params.showAngle || params.showAll || false
    }

    Update() {
        this.ActionInput()
        this.MovemmentInput()

        if(this.target) {
            this.UpdateAngle()
            this.UpdateTriangleRectangle()
            if(this.followTarget) this.FollowTarget()
        }
    }

    UpdateAngle() {
        //reference: https://gamedev.stackexchange.com/questions/14602/what-are-atan-and-atan2-used-for-in-games
        this.followAngle = 
        Math.atan2(
            this.target.position.y - this.y, 
            this.target.position.x - this.x
        ) * 180 / Math.PI
    }

    UpdateTriangleRectangle() {
        this.adjacentCato = Math.abs(this.x - this.target.position.x) 
        this.oppositeCato = Math.abs(this.y - this.target.position.y)
        
        // Math Formula: a^2 = b^2 + c^2
        this.hypotenuse = Math.sqrt(
            Math.pow(this.adjacentCato, 2) +
            Math.pow(this.oppositeCato, 2) 
        )
    }

    FollowTarget() {
        if(this.hypotenuse > this.radius) {
            const rad = this.followAngle * Math.PI / 180;
            this.Move(
                Math.cos(rad) * this.speed,
                Math.sin(rad) * this.speed
            )
        }
    }

    SetTarget(target) {
        this.target = target
    }

    ActionInput() {
        this.VerifyPressedAndDoAction("Space", "followBtnIsPressed", "followTarget")
        this.VerifyPressedAndDoAction("Digit1", "showHypotenuseBtnIsPressed", "showHypotenuse")
        this.VerifyPressedAndDoAction("Digit2", "showOppositeCatoBtnIsPressed", "showOppositeCato")
        this.VerifyPressedAndDoAction("Digit3", "showAdjacentCatoBtnIsPressed", "showAdjacentCato")
        this.VerifyPressedAndDoAction("Digit4", "showAngleBtnIsPressed", "showAngle")
    }

    VerifyPressedAndDoAction(key, pressedBtnName, actionName) {
        if(Keyboard.keyPressed[key]){ 
            if(!this[pressedBtnName]) {
                this[actionName] = !this[actionName]
                this[pressedBtnName] = true
            }
            return
         }

        this[pressedBtnName] = false
    }

    MovemmentInput() {
        if(Keyboard.keyPressed["KeyA"]){ this.ManualMove(-this.speed, 0) }
        if(Keyboard.keyPressed["KeyD"]){ this.ManualMove(this.speed, 0) }
        if(Keyboard.keyPressed["KeyW"]){ this.ManualMove(0, -this.speed) }
        if(Keyboard.keyPressed["KeyS"]){ this.ManualMove(0, this.speed) }
    }

    ManualMove(addX, addY) {
        this.followTarget = false
        this.Move(addX, addY)
    }

    Move(addX, addY){
        this.x += addX * Frame.DeltaTime();
        this.y += addY * Frame.DeltaTime();
    }

    Draw(ctx) {
        ctx.arc(this.x, this.y, this.radius, 0, 2 * Math.PI);
        ctx.fillStyle = "green"
        ctx.fill()
        ctx.stroke()
        
        if(this.showAngle) PlayerLines.DrawAngle(ctx, this, this.followAngle, true)
        
        if(this.showHypotenuse) PlayerLines.DrawHypotenuse(ctx, this, this.target, true)
        if(this.showOppositeCato) PlayerLines.DrawOppositeCato(ctx, this, this.target, true)
        if(this.showAdjacentCato) PlayerLines.DrawAdjacentCato(ctx, this, this.target, true)       
    }

}