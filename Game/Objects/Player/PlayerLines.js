class PlayerLines {}

PlayerLines.DrawHypotenuse = (ctx, from, to, debug) => {
    ctx.beginPath()

    ctx.moveTo(from.x, from.y)
    ctx.lineTo(to.position.x, to.position.y)
    ctx.strokeStyle = "#FF000077"
    ctx.lineWidth = 3
    ctx.stroke()
    ctx.closePath()
    ctx.restore()

    if(debug) {
        ctx.font = "10px Arial"
        ctx.textAlign = "center"
        ctx.fillStyle = "#000000"
        ctx.fillText(parseInt(from.hypotenuse), (from.x + to.position.x) / 2, (from.y + to.position.y) / 2)    
    }

    ctx.stroke()
    ctx.closePath()
}

PlayerLines.DrawOppositeCato = (ctx, from, to, debug) => {
    ctx.beginPath()
    
    ctx.moveTo(to.position.x, from.y)
    ctx.lineTo(to.position.x, to.position.y)
    ctx.strokeStyle = "#00000055"

    if(debug) {
        ctx.font = "10px Arial"
        ctx.textAlign = "center"
        ctx.fillStyle = "#00000055"
        ctx.fillText(parseInt(from.oppositeCato), to.position.x, (from.y + to.position.y) / 2)
    }

    ctx.stroke()
    ctx.closePath()
}

PlayerLines.DrawAdjacentCato = (ctx, from, to, debug) => {
    ctx.beginPath()
    
    ctx.moveTo(from.x, from.y)
    ctx.lineTo(to.position.x, from.y)
    ctx.strokeStyle = "#00000055"
    
    if(debug) {
        ctx.font = "10px Arial"
        ctx.textAlign = "center"
        ctx.fillStyle = "#00000055"
        ctx.fillText(parseInt(from.adjacentCato), (from.x + to.position.x) / 2, from.y)
    }

    ctx.stroke()
    ctx.closePath() 
}

PlayerLines.DrawAngle = (ctx, from, angle, debug) => {
    ctx.beginPath()

    ctx.arc(from.x, from.y, 30, 0, angle * Math.PI / 180 , true)
    ctx.strokeStyle = "#000000"
    
    if(debug) {
        const adjsutedAngle = angle < 0 ? 
            Math.abs(angle - 360) - 360 : 
            Math.abs(180 - angle) + 180

        ctx.font = "10px Arial"
        ctx.textAlign = "center"
        ctx.fillStyle = "#000000"
        ctx.fillText(
            parseInt(adjsutedAngle) + "º", 
            from.x + (35 * -Math.cos((angle) * Math.PI / 180)), 
            from.y + (35 * -Math.sin((angle) * Math.PI / 180))
        )
        // ctx.fillText(
        //     "atan2: " + parseInt(angle) + "º", 
        //     from.x + (45 * -Math.cos((angle) * Math.PI / 180)), 
        //     from.y + (45 * -Math.sin((angle) * Math.PI / 180))
        // )
    }

    ctx.stroke()
    ctx.closePath() 
}