class MouseFollower extends GameObject {

    Awake(params) {
        this.radius = 5
    }

    Draw(ctx) {
        ctx.arc(Mouse.position.x, Mouse.position.y, this.radius, 0, 2 * Math.PI);
        ctx.fillStyle = "red"
        ctx.fill()
        ctx.stroke()
    }

}