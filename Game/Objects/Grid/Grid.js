class Grid extends GameObject {

    Awake() {
        this.size = 50
        this.lines = Math.floor(canvas.height / this.size)
        this.coluns = Math.floor(canvas.width / this.size)

        this.color = "#00000020"
    }

    Draw(ctx) {

        ctx.beginPath()
    
        for(let i = 0; i < this.lines; i++){
            this.DrawLines(
                ctx,
                {x: 0, y: this.size * i}, 
                {x: canvas.width, y: this.size * i}
            )

            this.DrawNumber(ctx, this.size * i, {x: 0, y: this.size * i})
        }

        for(let j = 0; j < this.coluns; j++){
            this.DrawLines(
                ctx,
                {x: this.size * j, y: 0}, 
                {x: this.size * j, y: canvas.height}
            )

            this.DrawNumber(ctx, this.size * j, {x: this.size * j, y: 10})
        }
        
        ctx.stroke()
        ctx.closePath() 
    }

    DrawLines(ctx, from, to) {
        ctx.moveTo(from.x, from.y)
        ctx.lineTo(to.x, to.y)
        ctx.strokeStyle = this.color
    }

    DrawNumber(ctx, number, position) {
        ctx.font = "10px Arial"
        ctx.fillStyle = this.color
        ctx.textAlign = "left"
        ctx.fillText(number, position.x, position.y)
    }

}