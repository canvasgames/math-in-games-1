class GameObject {

    //TODO create a Rect extends GameObject, Tank, Cannon and Bullet extends Rect

    constructor(x, y, params){

        params = params || {}

        this.x = x || 0
        this.y = y || 0

        this.tag = params.tag
        this.hashCode = GameObjectManager.HashCode

        GameObjectManager.AddGameObject(this)
        this.Awake(params)
    }

    Awake(params) {}

    Update() {}

    Draw (ctx) {}

    Destroy() {
        GameObjectManager.DestroyGameObject(this)
    }
    
}